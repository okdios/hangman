package hangmangui;

/**
 *
 * @author okd
 */
public class Debug {
    private boolean debugEnabled;
    
    public Debug()
    {
        this.debugEnabled = false;
    }
    public Debug(boolean debugEnabled)
    {
        this.debugEnabled = debugEnabled;
    }
    
    public void enableDebug(boolean enable)
    {
        this.debugEnabled = enable;
    }
    
    public boolean isEnabled(boolean enable)
    {
        return this.debugEnabled;
    }
    
    public void print(String text)
    {
        if (this.debugEnabled)
        {
            System.out.println(text);
        }
    }
}
