package hangmangui;

import javax.swing.JOptionPane;

/**
 *
 * @author okd
 */
public class Hangman {
    
    private final WordGenerator wordGenerator;
    public Debug debug;
    private char[] usedLetters;
    private char[] wordArray;
    private char[] foundArray;
    private int remainingAttempts;
    private boolean gameOver;
    private final String EMPTY = "-";
    private final MainWindow mainWindow;
    private final String AVAILABLE_LETTERS = "qwertyuiopasdfghjklzxcvbnm";
    private final int WORD_GENERATOR_LIMIT = 10;
    HangmanDrawing drawing;
    
    //=================================================
    // Constructor
    //=================================================
    public Hangman()
    {
        this.debug = new Debug();
        this.debug.enableDebug(true);
        wordGenerator = new WordGenerator(this.debug);
        // start a new game
        this.newGame();
        this.drawing = new HangmanDrawing(this.debug);
        // create main window
        this.mainWindow = new MainWindow(this, this.debug);
        this.mainWindow.setBounds(0, 0, 530, 500);
        this.mainWindow.setResizable(false);
        this.mainWindow.setVisible(true);
        this.drawing.setBounds(0, 0, 530, 300);
        this.mainWindow.add(drawing);
    }
    
    
    //=================================================
    // Start a new game
    //=================================================
    public void newGame()
    {
        String word = wordGenerator.getRandomWord();
        int i = 0;
        while (this.isWordValid(word) == false)
        {   
            this.debug.print(
                      "The word " + word + " is invalid. Trying with "
                    + "another word. " + (this.WORD_GENERATOR_LIMIT-i) + " "
                    + "attempts left.");
            i++;
            if (i > this.WORD_GENERATOR_LIMIT)
            {
                JOptionPane.showMessageDialog(null, String.format(
                          "The word list appears to be invalid! Please "
                        + "find another list, and start the game again.", 
                            null));
            }
            // make sure the word is valid
            word = wordGenerator.getRandomWord();
        }
        this.usedLetters = new char[0];
        this.wordArray = new char[word.length()];
        this.fillFoundArray(word.length());
        this.createWordArray(word);
        this.remainingAttempts = 10;
        this.gameOver = false;
        //this.drawing.reset();
        //this.drawing.setGameOver(false);
        //this.drawing.setWon(false);
    }
    
    //=================================================
    // Check if a letter is in list of available letters (char)
    //=================================================
    public boolean isLetterValid(char letter)
    {
        return this.charArrayContains(this.AVAILABLE_LETTERS.toCharArray(), letter);
    }
    
    
    //=================================================
    // Check if a letter is in list of available letters (String)
    //=================================================
    public boolean isLetterValid(String letterAsString)
    {
        char letter = letterAsString.toLowerCase().charAt(0);
        return this.isLetterValid(letter);
    }
    
    
    //=================================================
    // Check if a word only contains valid letters
    //=================================================
    public boolean isWordValid(String word)
    {
        for (char letter : word.toLowerCase().toCharArray())
        {
            if (this.charArrayContains(this.AVAILABLE_LETTERS.toCharArray(), letter) == false)
            {
                return false;
            }
        }
        return true;
    }
    
    
    private String charArrayAsString(char[] chars)
    {
        StringBuilder s = new StringBuilder();
        for (char c : chars)
        {
            s.append(c);
        }
        return s.toString();
    }
    
    public String getUsedLettersAsString()
    {
        return "Not in word: " + this.charArrayAsString(this.usedLetters);
    }
    
    public String getWordAsString()
    {
        return this.charArrayAsString(this.wordArray);
    }
    
    public String getFoundAsString()
    {
        return this.charArrayAsString(this.foundArray);
    }
    
    private void fillFoundArray(int size)
    {
        char[] foundArray = new char[size];
        int i = 0;
        for (char c : foundArray)
        {
            foundArray[i] = this.EMPTY.charAt(0);
            i++;
        }
        this.foundArray = foundArray;
    }
    
    private void addAttempt()
    {
        this.remainingAttempts = this.remainingAttempts - 1;
        if (this.remainingAttempts <= 0)
        {
            this.gameOver = true;
        }
    }
    
    public boolean isGameOver()
    {
        return this.gameOver;
    }
    
    public boolean won()
    {
        if (this.charArrayContains(this.foundArray, this.EMPTY.charAt(0)))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    public int getRemainingAttempts()
    {
        return this.remainingAttempts;
    }
    
    public String getRemainingAttemptsAsString()
    {
        return "Attempts left: " + this.remainingAttempts;
    }
    
    public void printRemainingAttempts()
    {
        System.out.println("Remaining attempts: " + this.remainingAttempts);
    }
    
    private void createWordArray(String word)
    {
        this.wordArray = word.toCharArray();
    }
    
    public char[] getFoundWord()
    {
        return this.foundArray;
    }
    
    public void printFoundWord()
    {
        StringBuilder s = new StringBuilder();
                
        for (char c : this.getFoundWord())
        {
           s.append(c); 
        }
        System.out.println("Found letters so far: " + s.toString());
    }
    
    public char[] getUsedLetters()
    {
        return this.usedLetters;
    }
    
    public void printUsedLetters()
    {
        StringBuilder s = new StringBuilder();
                
        for (char c : this.getUsedLetters())
        {
           s.append(c); 
        }
        System.out.println("Used letters: " + s.toString());
    }
    
    private void addUsedLetter(char letter)
    {
        // if not already in array...
        if (this.charArrayContains(this.usedLetters, letter) == false)
        {
            this.debug.print("adding " + letter);
            
            int currentSize = this.getUsedLetters().length;
            char[] newArray = new char[currentSize + 1];
            int i = 0;
            // add all current letters to the new array
            for (char c : this.usedLetters)
            {
                newArray[i++] = c;
            }
            // add new letter to array
            newArray[currentSize] = letter;
            
            // Set the new array as array of used letters
            this.usedLetters = newArray;       
            
            this.addAttempt();
        }
    }
    
    private void addFoundLetter(char letter)
    {   
        for (int i : this.getPositionsOfLetter(letter))
        {
            this.foundArray[i] = letter;
        }
        if (this.charArrayContains(this.foundArray, this.EMPTY.charAt(0)) == false)
        {
            this.gameOver = true;
        }
    }
    
    private int[] getPositionsOfLetter(char letter)
    {
        int[] array = new int[0];
        int i = 0;
        for (char c : this.wordArray)
        {
            if (c == letter)
            {
                array = this.addToIntArray(array, i);
            }
            i++;
        }
        return array;
    }
    
    private int[] addToIntArray(int[] array, int value)
    {
        int currentSize = array.length;
        int[] newArray = new int[currentSize + 1];
        int i = 0;
        // add all current letters to the new array
        for (int v : array)
        {
            newArray[i++] = v;
        }
        // add new value to array
        newArray[currentSize] = value;
            
        // Set the new array as array of used letters
        return newArray;
    }
    
    /*
    private String charToSTring(char[] chars)
    {
        StringBuilder s = new StringBuilder();
        for (char c : chars)
        {
            s.append(c);
        }
        return s.toString();
    }
    * */
    
    private boolean charArrayContains(char[] array, char letter)
    {
        for (char c : array)
        {
            if (c == letter) 
            {
                return true;
            }
        }
        return false;
    }
    
    public boolean checkLetter(char letter)
    {
        boolean isMatch = this.charArrayContains(wordArray, letter);
        
        // add to found letters, if word contain this letter
        if (isMatch)
        {   // update word with found letter(s)
            this.addFoundLetter(letter);
        }
        else
        {// add to already used letters, and remove 1 from remaining attempts
            this.addUsedLetter(letter);
        }
        
        String debugText = "No match";
        if (isMatch)
            debugText = "Match!";
        this.debug.print("Checking the letter " + letter + " : " + debugText +  "");
     
        
        
        // check if the word contains this letter
        return isMatch;
    }
    
    public boolean checkLetter(String letterAsString)
    {
        // convert letter to lower case char
        char letter = letterAsString.toLowerCase().charAt(0);
        return this.checkLetter(letter);
    }
    
    /*private void printDebug(String text)
    {
        if (this.debug)
        {
            System.out.println(text);
        }
    }*/
}
