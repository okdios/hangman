/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hangmangui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JPanel;

/**
 *
 * @author okd
 */
public class HangmanDrawing extends JPanel {
    
    private Color HANGMAN_COLOR = Color.BLACK;
    private int attemptsLeft;
    public Debug debug;
    private boolean writeGameOver;
    private boolean writeYouWon;
    
    public HangmanDrawing(Debug debug)
    {
        this.debug = debug;
        this.attemptsLeft = 10;
        this.reset();
    }
    
    public void reset()
    {
        this.writeGameOver = false;
        this.writeYouWon = false;
    }
    
    protected void paintComponent(Graphics g)
    {
        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        super.paintComponent(g);
        this.drawComponents(g);
    }
    
    public void drawComponents(Graphics g)
    {
        this.debug.print("drawComponent, attempt: " + this.attemptsLeft);
        int centerX = this.getWidth() / 2;
        int centerY = this.getHeight() / 2;
        int groundLevel = this.getHeight() - 20;
        int pileX = centerX - 50;
        int pileHeight = 200;
        int pileWidth = 100;
        int ropeLength = 20;
        int headRadius = 15;
        int bodyLength = 80;
        int armLength = 30;
        int armPosFromHead = 10;
        int handYDiff = 10;
        int legLength = 50;
        int legXDiff = 30;
        
        //this.attemptsLeft = 1;
        if (this.attemptsLeft <= 9)
        {   // Ground
            g.setColor(this.HANGMAN_COLOR);
            g.drawLine(
                    centerX-150, 
                    groundLevel, 
                    centerX+150, 
                    groundLevel);
        }
        if (this.attemptsLeft <= 8)
        {   // Pile
            g.setColor(this.HANGMAN_COLOR);
            g.drawLine(
                    pileX, 
                    groundLevel, 
                    pileX, 
                    groundLevel - pileHeight);
        }
        if (this.attemptsLeft <= 7)
        {   // Pile top
            g.setColor(this.HANGMAN_COLOR);
            g.drawLine(
                    pileX, 
                    groundLevel - pileHeight, 
                    pileX + pileWidth, 
                    groundLevel - pileHeight);
        }
        if (this.attemptsLeft <= 6)
        {   // Rope
            g.setColor(this.HANGMAN_COLOR);
            g.drawLine(
                    pileX + pileWidth, 
                    groundLevel - pileHeight, 
                    pileX + pileWidth, 
                    groundLevel - pileHeight + ropeLength);
        }
        if (this.attemptsLeft <= 5)
        {   // Head
            g.setColor(this.HANGMAN_COLOR);
            g.drawOval(
                    pileX + pileWidth - headRadius, 
                    groundLevel - pileHeight + ropeLength, 
                    2*headRadius, 
                    2*headRadius);
        }
        if (this.attemptsLeft <= 4)
        {   // Body
            g.setColor(this.HANGMAN_COLOR);
            g.drawLine(
                    pileX + pileWidth, 
                    groundLevel - pileHeight + ropeLength + (2*headRadius), 
                    pileX + pileWidth, 
                    groundLevel - pileHeight + ropeLength + (2*headRadius) + bodyLength);
        }
        if (this.attemptsLeft <= 3)
        {   // Left Arm
            g.setColor(this.HANGMAN_COLOR);
            g.drawLine(
                    pileX + pileWidth, 
                    groundLevel - pileHeight + ropeLength + (2*headRadius) + armPosFromHead, 
                    pileX + pileWidth - armLength, 
                    groundLevel - pileHeight + ropeLength + (2*headRadius) + armPosFromHead + handYDiff);
        }
        if (this.attemptsLeft <= 2)
        {   // Right Arm
            g.setColor(this.HANGMAN_COLOR);
            g.drawLine(
                    pileX + pileWidth, 
                    groundLevel - pileHeight + ropeLength + (2*headRadius) + armPosFromHead, 
                    pileX + pileWidth + armLength, 
                    groundLevel - pileHeight + ropeLength + (2*headRadius) + armPosFromHead + handYDiff);
        }
        if (this.attemptsLeft <= 1)
        {   // Left leg
            g.setColor(this.HANGMAN_COLOR);
            g.drawLine(
                    pileX + pileWidth, 
                    groundLevel - pileHeight + ropeLength + (2*headRadius) + bodyLength, 
                    pileX + pileWidth - legXDiff, 
                    groundLevel - pileHeight + ropeLength + (2*headRadius) + bodyLength + legLength);
        }
        if (this.attemptsLeft <= 0)
        {   // Right leg
            g.setColor(this.HANGMAN_COLOR);
            g.drawLine(
                    pileX + pileWidth, 
                    groundLevel - pileHeight + ropeLength + (2*headRadius) + bodyLength, 
                    pileX + pileWidth + legXDiff, 
                    groundLevel - pileHeight + ropeLength + (2*headRadius) + bodyLength + legLength);
        }
        if (this.writeYouWon)
        {
            g.setFont(new Font("Courier", Font.BOLD, 40));
            g.setColor(Color.GREEN);
            g.drawString("You won!", centerX-100, centerY);
        }
        else if (this.writeGameOver)
        {
            g.setFont(new Font("Courier", Font.BOLD, 40));
            g.setColor(Color.RED);
            g.drawString("Game Over!", centerX-100, centerY);
        }
    }
    
    public void setAttempt(int attemptsLeft)
    {
        this.debug.print("Setting attemptsLeft to " + attemptsLeft);
        this.attemptsLeft = attemptsLeft;
    }
    public void setGameOver(boolean isGameOver)
    {
        this.writeGameOver = isGameOver;
    }
    public void setWon(boolean hasWon)
    {
        this.writeYouWon = hasWon;
    }
}
